->Developed a Backup system using the tree data structure for the student course registration system in java. 

-> Used the prototype design pattern ( https://en.wikipedia.org/wiki/Prototype_pattern ) for replicating the data 
   in the main tree to create multiple Backup trees( backup modules are clones of the original tree). 

-> Also used the Observer Design Pattern ( https://en.wikipedia.org/wiki/Observer_pattern ) for making 
   real-time updates to all the backup modules upon any changes to the original tree.
   
-----------------------
Added visitors to the trees using the visitor pattern.

https://bitbucket.org/krishnasumedh_m/backup-visitors





