package studentCoursesBackup1.driver;
import java.util.ArrayList;

import studentCoursesBackup1.util.FileDisplayInterface;
import studentCoursesBackup1.util.FileProcessor;
import studentCoursesBackup1.util.Results;
import studentCoursesBackup1.util.TreeBuilder;

public class Driver 
{
	String line = null;

	public static void main(String[] args) 
	{	
		ArrayList<Results> res;
		FileProcessor fp,fp1;
		FileDisplayInterface fdIn;
		  
		if( (args.length != 5))
		   {
			 System.err.println("Invalid Arguments! Format : <input.txt> <delete.txt> <output1.txt> <output2.txt> <output3.txt> \n");
			 System.exit(0);
		   }
		
		 
		  TreeBuilder tb = new TreeBuilder();
		   
		  fp = new FileProcessor(args[0]);
		  tb.readL(fp);
		  
		  fp1 = new FileProcessor(args[1]);
		  tb.readDelete(fp1);
		  
		  
		  int i=1;
		  res = tb.getResultsInstance();
		  
		  for(Results R :res)
		  {
			  fdIn = R;
			  String line = R.getResult();
			  i++;
			  
			  fdIn.writeToFile(args[i]);
			  if(i==5)
				  break;
		  }
		  

	}

}
