package studentCoursesBackup1.myTree;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;


public class Node implements ObserverI,SubjectI,Cloneable
{
    private int B_Number;
    //Data Structure for Courses
    Set<String> courses = new HashSet<String>();
    //Data structure for listeners
    ArrayList<ObserverI> observers = new ArrayList<ObserverI>();
    Node leftnode,rightnode ;
    
    public Node(int bn, String course)
    {
    	B_Number = bn;
    	courses.add(course);
    	leftnode = null;
    	rightnode = null;
    }

	@Override
	public void registerObserver(ObserverI o) 
	{
		observers.add(o);
		
	}
	
	public Object clone() throws CloneNotSupportedException
	{  
	    	return super.clone();  
	}  
	  
	  

	@Override
	public void removeObserver(ObserverI o) 
	{
		observers.remove(o);
		
	}

	@Override
	public void notifyObserver(Object o) 
	{
		    update(o);	
	}

	@Override
	public void update(Object o) 
	{
		Updates up = (Updates) o ;
		String operation = up.getOperation();
		
		if(operation.equals("add"))
		{
			for(ObserverI in :observers)
			{   Node n = (Node)in;
			    n.addCourse( up.getCourse() );	
			} 
		}
		
		else
		{
			for(ObserverI in :observers)
			{   Node n = (Node)in;
			    n.deleteCourse( up.getCourse() );
				
			} 	
			
		}
		
	}
	
	public int getBNumber()
	{
		return B_Number;	
	}
	
	public Node getLeftNode()
	{
		return leftnode;
		
	}
	
	public Node getRightNode()
	{
		return rightnode;
		
	}
	
	public void setLeftNode(Node node)
	{
	  leftnode =  node;
		
	}
	
	public void setRightNode(Node node)
	{
		rightnode = node ;
		
	}
	
	public void addCourse(String course)
	{
		courses.add(course);
	}
	
	
	public void deleteCourse(String course)
	{
		courses.remove(course);
	}
	
	
	public Set<String>  getCourses()
	{
		return courses;
		
	}


}
